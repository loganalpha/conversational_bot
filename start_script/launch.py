#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on 2020 September 27 20:07:10 (EST) 

@author: KanExtension
"""

import os
import sys
import tempfile
from typing import List, Tuple
import subprocess
import yaml

'''
H E L P E R   F U N C T I O N S
'''


class CustomPopen():
    """
    This emulates the subproces.Popen API
    """
    class StdOut():
        def __init__(self, stdout: List[str]):
            self._stdout = stdout

        def readlines(self) -> List[str]:
            return self._stdout

    class StdErr():
        def __init__(self, stderr: List[str]):
            self._stderr = stderr

        def readlines(self) -> List[str]:
            return self._stderr

    def __init__(self, shell_object: subprocess.Popen,
                 stdout: List[str], stderr: List[str]):
        self._shell_object = shell_object

        self.stdout = CustomPopen.StdOut(stdout)
        self.stderr = CustomPopen.StdErr(stderr)

    def wait(self) -> int:
        return self._shell_object.wait()


def _command_executor(cmd: str, shell_path: str):
    """This is a normal command executor, except it implements the issue discussed here:
    https://thraxil.org/users/anders/posts/2008/03/13/Subprocess-Hanging-PIPE-is-your-enemy/

    Parameters
    ----------
    cmd
    shell_path

    Returns
    -------

    """
    _tempf, _tempf_e = tempfile.TemporaryFile(), tempfile.TemporaryFile()

    _shell_object = subprocess.Popen([shell_path, '-c', cmd], stdout=_tempf, stderr=_tempf_e)
    _shell_object.wait()

    _tempf.seek(0)
    _tempf_e.seek(0)

    _pipe_out, _tempf_out_e = list(_tempf), list(_tempf_e)

    _tempf.close()
    _tempf_e.close()

    return CustomPopen(_shell_object, _pipe_out, _tempf_out_e)


def start_action_server(shell_path='/bin/bash') -> dict:
    """

    Parameters
    ----------
    shell_path

    Returns
    -------

    """
    # -- functions --
    _zsh_command = lambda cmd: _command_executor(cmd, shell_path)

    # -- step 0 -- run the last one, the server creation, must be in tmux
    _logs = {}

    # -- step 1 --  start the  tmux session
    _tmux_session_name = 'bot_actions'

    _tmux_session = f"""tmux new -s '{_tmux_session_name}' -d"""

    _result = _zsh_command(_tmux_session)
    _result.wait()

    # -- step 1 -- inject the server run
    _tmux_command = lambda cmd: f"""tmux send-keys -t '{_tmux_session_name}' '{cmd}' C-m"""

    # command that starts the actions
    _action_cmd = 'cd /conversational_bot && rasa run actions -vv'

    # run the tmux command
    _result = _zsh_command(_tmux_command(_action_cmd))

    _result.wait()

    _logs = {**_logs, _action_cmd: _result.stderr.readlines()}

    return _logs


def start_chatbot_server(chat_model: str, shell_path='/bin/bash') -> dict:
    """

    Parameters
    ----------
    chat_model
    shell_path

    Returns
    -------

    """
    # -- functions --
    _zsh_command = lambda cmd: _command_executor(cmd, shell_path)

    # -- step 0 -- run the last one, the server creation, must be in tmux
    _logs = {}

    # -- step 1 --  start the  tmux session
    _tmux_session_name = 'bot'

    _tmux_session = f"""tmux new -s '{_tmux_session_name}' -d"""

    _result = _zsh_command(_tmux_session)
    _result.wait()

    # -- step 1 -- inject the server run
    _tmux_command = lambda cmd: f"""tmux send-keys -t '{_tmux_session_name}' '{cmd}' C-m"""

    # command that starts the actions
    _action_cmd = f'cd /conversational_bot && rasa run -m models/{chat_model} --enable-api --cors "*" --debug --endpoints endpoints.yml --credentials credentials.yml'

    # cd /conversational_bot && rasa run -m models/20201004-035730.tar.gz --enable-api --cors "*" --debug --endpoints endpoints.yml --credentials credentials.yml

    # run the tmux command
    _result = _zsh_command(_tmux_command(_action_cmd))

    _result.wait()

    _logs = {**_logs, _action_cmd: _result.stderr.readlines()}

    return _logs

def kill_tmux_sessions(shell_path='/bin/bash') -> dict:
    """This function goes and kills all the tmux sessions

    Parameters
    ----------
    shell_path

    Returns
    -------

    """

    # -- functions --
    _zsh_command = lambda cmd: _command_executor(cmd, shell_path)

    # -- step 0 -- the killing command
    _logs = {}

    _kill_tmux_cmd = 'tmux list-sessions | grep -v attached | cut -d: -f1 | xargs -t -n1 tmux kill-session -t'

    # -- step 1 -- execute tmux
    _result = _zsh_command(_kill_tmux_cmd)
    _result.wait()

    _logs = {**_logs, _kill_tmux_cmd: _result.stderr.readlines()}

    return _logs


'''
M A I N   F U N C T I O N S
'''


def main():

    # -- step 0 -- open the local config file; we know, it resides in: '/conversational_bot/__temp__/config.md'
    # because the shell creator dumps it there
    with open('/conversational_bot/__temp__/config.md', 'r') as handle:
        _config = yaml.safe_load(handle)

    # -- step 1 -- kill the tmux session, if there is some
    _ = kill_tmux_sessions()

    # -- step 2 -- having the config in hand we can load the concrete model action server
    _ = start_action_server()

    # -- step 3 -- start actual rasa
    _ = start_chatbot_server(_config["model_name"])


if __name__ == '__main__':
    main()





